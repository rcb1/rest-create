package com.learning.spring.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.learning.spring.entity.Student;

@RestController
@RequestMapping("/api")
public class StudentController {

	// Returns list of students
	@GetMapping("/student")
	public List<Student> getStudent() {
		Scanner s = new Scanner(System.in);
		System.out.print("Enter no of Stundets: ");
		int n = s.nextInt();
		Student[] st = new Student[n];
		for (int i = 0; i < n; i++) {
			System.out.println((i + 1) + " - Student Details");
			System.out.print("Enter first name: ");
			String a = s.next();
			System.out.print("Enter last name: ");
			String b = s.next();
			s.close();
			st[i] = new Student(a, b);
		}

		List<Student> student = new ArrayList<Student>();
		for (int j = 0; j < n; j++) {
			student.add(st[j]);
		}
		return student;
	}
}
